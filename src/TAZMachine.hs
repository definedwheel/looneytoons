module TAZMachine (
  emptyTAZM
, ready
, execute
) where

import Imports hiding (length)
import Core


-- | Makes a TAZ machine from a TAZ program, and an input list.
ready :: TAZProgram -> TAZMachine
ready tp = let len = length tp
               hb = fromIntegral $ len-1
               mem = array (0, hb) tp
           in TAZM mem 0 False [] []


emptyTAZM = TAZM (array (0,0) [(0, int2tw 0)]) 0 False [] [] 


pop :: [TAZInput] -> (Maybe TAZInput, [TAZInput])
pop l
  | null l = (Nothing, l)
  | otherwise = (Just $ head l, tail l)


execute :: TAZMachine -> TAZMachine
execute tm@(TAZM m a h i o) =
  let instr@(I op a1 a2 a3) = tw2ins $ m!a
      mip = snd $ bounds m
      nextaddr = if a == mip then 0 else a+1
      isValid = a1 <= mip && a2 <= mip && a3 <= mip
  in if h || not isValid then tm {isHalt = True}
     else case instr of
       (I Nop _ _ _)
         -> tm {addrp = nextaddr}

       (I Ini d _ _)
         -> case pop i of
              (Nothing, l) -> tm {isHalt = True}
              (Just (IF v), l) -> tm {isHalt = True}
              (Just (II v), l) -> let ntw = int2tw v
                                  in tm { mem = m // [(d, ntw)]
                                        , inpList = l
                                        , addrp = nextaddr
                                        }

       (I Oui s _ _)
         -> let no = o ++ [OI $ tw2int $ m!s]
            in tm { outList = no
                  , addrp = nextaddr
                  }

       (I Inf d _ _)
         -> case pop i of
              (Nothing, l) -> tm {isHalt = True}
              (Just (II v), l) -> tm {isHalt = True}
              (Just (IF v), l) -> let ntw = flt2tw v
                                  in tm { mem = m // [(d, ntw)]
                                        , inpList = l
                                        , addrp = nextaddr
                                        }

       (I Ouf s _ _)
         -> let no = o ++ [OF (tw2flt (m!s))]
            in tm {outList = no
                  , addrp = nextaddr
                  }

       (I Mov s d _)
         -> tm { mem = m // [(d, m!s)]
               , addrp = nextaddr}

       (I Swp a1 a2 _)
         -> tm {mem = m // [(a1, m!a2)
               , (a2, m!a1)]
               , addrp = nextaddr}

       (I Jmp d _ _)
         -> tm {addrp = d}

       (I Jeq a1 a2 d)
         -> tm {addrp = if tw2int (m!a1) == tw2int (m!a2) then d
                        else nextaddr}

       (I Jne a1 a2 d)
         -> tm {addrp = if tw2int (m!a1) /= tw2int (m!a2) then d
                        else nextaddr}

       (I Jls a1 a2 d)
         -> tm {addrp = if tw2int (m!a1) <  tw2int (m!a2) then d
                        else nextaddr}

       (I Jgs a1 a2 d)
         -> tm {addrp = if tw2int (m!a1) >  tw2int (m!a2) then d
                        else nextaddr}

       (I Jle a1 a2 d)
         -> tm {addrp = if tw2int (m!a1) <= tw2int (m!a2) then d
                        else nextaddr}

       (I Jge a1 a2 d)
         -> tm {addrp = if tw2int (m!a1) >= tw2int (m!a2) then d
                        else nextaddr}

       (I Add a1 a2 d)
         -> let ntw = int2tw (tw2int (m!a1) + tw2int (m!a2))
            in tm { mem = m // [(d, ntw)]
                  , addrp = nextaddr
                  }

       (I Sub a1 a2 d)
         -> let ntw = int2tw (tw2int (m!a1) - tw2int (m!a2))
            in tm { mem = m // [(d, ntw)]
                  , addrp = nextaddr
                  }

       (I Mul a1 a2 d)
         -> let ntw = int2tw (tw2int (m!a1) * tw2int (m!a2))
            in tm { mem = m // [(d, ntw)]
                  , addrp = nextaddr
                  }

       (I Div a1 a2 d)
         -> if tw2int (m!a2) == 0 then tm {isHalt = True}
            else let ntw = int2tw (tw2int (m!a1) `div` tw2int (m!a2))
                 in tm { mem = m // [(d, ntw)]
                       , addrp = nextaddr
                       }

       (I Mod a1 a2 d)
         -> if tw2int (m!a2) == 0 then tm {isHalt = True}
            else let ntw = int2tw (tw2int (m!a1) `mod` tw2int (m!a2))
                 in tm { mem = m // [(d, ntw)]
                       , addrp = nextaddr
                       }

       (I And a1 a2 d)
        -> let ntw = int2tw (tw2int (m!a1) .&. tw2int (m!a2))
           in tm { mem = m // [(d, ntw)]
                 , addrp = nextaddr
                 }

       (I Or a1 a2 d)
         -> let ntw = int2tw (tw2int (m!a1) .|. tw2int (m!a2))
            in tm { mem = m // [(d, ntw)]
                  , addrp = nextaddr
                  }

       (I Xor a1 a2 d)
         -> let ntw = int2tw (tw2int (m!a1) `xor` tw2int (m!a2))
            in tm { mem = m // [(d, ntw)]
                  , addrp = nextaddr
                  }

       (I Not a1 d _)
         -> let ntw = int2tw (complement $ tw2int (m!a1))
            in tm { mem = m // [(d, ntw)]
                  , addrp = nextaddr
                  }

       (I Shf a1 c d)
         -> let iop = tw2int $ m!a1
                ish = fromIntegral $ tw2int $ m!c
                ntw = int2tw $ shift iop ish
            in tm { mem = m // [(d, ntw)]
                  , addrp = nextaddr
                  }

       (I Rot a1 c d)
         -> let iop = tw2int $ m!a1
                irt = fromIntegral $ tw2int $ m!c
                ntw = int2tw $ rotate iop irt
            in tm { mem = m // [(d, ntw)]
                  , addrp = nextaddr
                  }

       (I Fad a1 a2 d)
         -> let ntw = flt2tw (tw2flt (m!a1) + tw2flt (m!a2))
            in tm { mem = m // [(d, ntw)]
                  , addrp = nextaddr
                  }

       (I Fsu a1 a2 d)
         -> let ntw = flt2tw (tw2flt (m!a1) - tw2flt (m!a2))
            in tm { mem = m // [(d, ntw)]
                  , addrp = nextaddr
                  }

       (I Fmu a1 a2 d)
         -> let ntw = flt2tw (tw2flt (m!a1) * tw2flt (m!a2))
            in tm { mem = m // [(d, ntw)]
                  , addrp = nextaddr
                  }

       (I Fdi a1 a2 d)
         -> let ntw = flt2tw (tw2flt (m!a1) / tw2flt (m!a2))
            in tm { mem = m // [(d, ntw)]
                  , addrp = nextaddr
                  }

       (I Itf a1 d _)
         -> let ntw = flt2tw (fromIntegral $ tw2int (m!a1))
            in tm { mem = m // [(d, ntw)]
                  , addrp = nextaddr
                  }

       (I Fti a1 d _)
         -> let ntw = int2tw (truncate $ tw2flt (m!a1))
            in tm { mem = m // [(d, ntw)]
                  , addrp = nextaddr
                  }

       (I Hlt _ _ _)
         -> tm {isHalt = True}
