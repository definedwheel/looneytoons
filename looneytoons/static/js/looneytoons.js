var tazmachine = {}
var views = []
var fr = new FileReader()

$(function(){
  initStorage();
  drawUI(false);
})

function initStorage() {
  if (sessionStorage.length = 0) {
    sessionStorage.setItem(0, [0,0,0,0]);
  }
}

function drawUI(exec) {
  $("body").empty()
  drawHeader(exec);
  drawMemory(exec);
}

function drawHeader(exec) {
  $body = $("body");
  $header = $("<header>")
  $body.append($header);

  $header.append("<button type='button' id='open'>Open</button>");
  $header.append("<input id='file-open' type='file' style='display: none;'></input>");

  $header.append("<button id='edit'>Edit</button>");
  $header.append("<button id='run'>Run</button>");

  $header.append("<a id='savelink' id='file-save'><button id='save'>Save</button></input>");

  $edit = $("#edit");
  $run = $("#run");
  $open = $("#open");
  $fopen = $("#file-open");
  $save = $("#save");
  $fsave = $("#file-save");

  if (exec) {
    $run.addClass("selected");
    $edit.addClass("notselected");
  } else {
    $run.addClass("notselected");
    $edit.addClass("selected");
  }

  $edit.click(function(){drawUI(false)});
  $run.click(function(){
    if (sessionStorage.length > 0) {
      $.post("/compile", JSON.stringify(storage2tp()), function(r){
        if (r.ok) {
          tazmachine = r.tm
          drawUI(true);
        } else {
          showMessage(r.error);      
        }
      });
    }
  });

  $open.click(function(){$fopen.trigger("click")});
  $save.click(function(){
    s = new Uint8Array(sessionStorage.length*4)
    $.each(storage2tp(), function (ix, e) {
      s[ix*4+0] = e[0]
      s[ix*4+1] = e[1]
      s[ix*4+2] = e[2]
      s[ix*4+3] = e[3]
    })
    b = new Blob([new Uint8Array([0x54, 0x41, 0x5a, 0x21]), s], {type: "application/octet-stream"})
    u = window.URL.createObjectURL(b)
    $("#savelink").prop("download", "tazp");
    $("#savelink").prop("href", u);
    $("#savelink").trigger("click");
    // FIXME: revoke url
  });
  $fopen.change(function(){
    f = $("#file-open").get(0).files[0];
    fr.onload = function(){
      $("#file-open").val("")
      r = fr.result;
      a = new Uint8Array(r);
      ar = Array.from(a)
      ntp = []
      for (i=0, j=ar.length; i < j; i+=4) {ntp[i/4] = ar.slice(i, i+4)}
      magic = ntp.splice(0, 1)
      if (magic[0].toString() != "84,65,90,33") {
        showMessage("Bad magic.")
      } else {
        tp2storage(Object(ntp));
        views = []
        drawUI(false);
      }
    }
    fr.onerror = function(){showMessage("Can't open file.")};
    fr.readAsArrayBuffer(f);
  })
}


function drawMemory(exec) {
  if (exec) {
    $("body").append("<div id='imo'></div>");
    $imo = $("#imo");
    $input = $("<ul>", {id: 'input'});
    $memory = $("<ul>", {id: 'memory'});
    $memory.prop("width", "33%")
    $output = $("<ul>", {id: 'output'});
    $imo.append($input);
    $imo.append($memory);
    $imo.append($output);

    $addinput = $("<button>", {class: "add", text: "+"});
    $delinput = $("<button>", {class: "deleteIO", text: "-"});
    $input.append($addinput);
    $input.append($delinput);
    $.each(tazmachine.input, function(i, e) {
      $input.append(newInput(i, e));
    })
    $.each(storage2tp(), function(addr, tw) {
      $memory.append(newWord(addr, tw, true, tazmachine.addr));
    })
    $.each(tazmachine.output, function(i, e) {
      $output.append(newOutput(i, e));
    })
    $addinput.click(function(){
      $input.append(newInput(tazmachine.input.length, {tag: "II", contents: 0}));
      tazmachine.input.push({tag: "II", contents: 0})
    })
    $delinput.click(function(){
      $input.children('.io').first().remove()
      tazmachine.input.splice(0,1);
    })
  } else {
    $("body").append("<ul id='memory'></ul>");
    $memory = $("#memory");
    $.each(storage2tp(), function(addr, tw) {
      $memory.append(newWord(addr, tw, false));
    })
    $memory.append("<button class='add'>+</button>");
    $(".add").click(function(){
      if (sessionStorage.length < 256) {
        $(".add").before(newWord(sessionStorage.length, [0,0,0,0], false));
        sessionStorage.setItem(sessionStorage.length, [0,0,0,0])
      }
    });
    if (sessionStorage.length == 256) {$("#add").prop("disabled", true)}

    if (!exec) {
      $memory.sortable({
        axis: "y",
        containment: "parent",
        items: "> .word",
        scroll: true,
        update: function (event, ui) {
          oldaddr = ui.item[0].dataset.addr
          newaddr = $(".word[data-addr="+oldaddr+"]").index()
          a = $.makeArray(sessionStorage);
          w = a.splice(oldaddr, 1);
          wv = views.splice(oldaddr, 1)
          a.splice(newaddr,0,w[0])
          views.splice(newaddr,0,wv[0])
          tp2storage(Object(a));
          drawUI(false); // TODO: full redrawing is expensive, think about DOM manipulation
        }
      })
    }
  }
}

function newWord(addr, tw, exec, ip) {
  $newword = $("<li>", {class:'word', 'data-addr': addr});
  $index = $("<div>", {class: 'index', text: addr});
  if (exec) {val = tazmachine.memory[addr]} else {val = tw}
  $value = $("<div>", {class: 'value', text: convert(val, views[addr] ? views[addr] : "instr")});
  $newword.append($index);
  $newword.append($value);

  $views = $("<div>", {class:'views'});
  if (views[addr] == "int32") {
    $instr = $("<button>", {class:'instr vnotselected'});
    $int32 = $("<button>", {class:'int32 vselected'});
    $float = $("<button>", {class:'float vnotselected'});
    views[addr] = "int32"
  } else if (views[addr] == "float") {
    $instr = $("<button>", {class:'instr vnotselected'});
    $int32 = $("<button>", {class:'int32 vnotselected'});
    $float = $("<button>", {class:'float vselected'});
    views[addr] = "float"
  } else {
    $instr = $("<button>", {class:'instr vselected'});
    $int32 = $("<button>", {class:'int32 vnotselected'});
    $float = $("<button>", {class:'float vnotselected'});
    views[addr] = "instr"
  }
  $views.append($instr);
  $views.append($int32);
  $views.append($float);
  $newword.append($views);

  $value = $(".word[data-addr='"+addr+"'] > .value")

  $instr.click(function(){
    $(".word[data-addr='"+addr+"'] > .views > .instr").toggleClass("vselected", true);
    $(".word[data-addr='"+addr+"'] > .views > .instr").toggleClass("vnotselected", false);
    $(".word[data-addr='"+addr+"'] > .views > .int32").toggleClass("vselected", false);
    $(".word[data-addr='"+addr+"'] > .views > .int32").toggleClass("vnotselected", true);
    $(".word[data-addr='"+addr+"'] > .views > .float").toggleClass("vselected", false);
    $(".word[data-addr='"+addr+"'] > .views > .float").toggleClass("vnotselected", true);
    if (exec) {
      $(".word[data-addr='"+addr+"'] > .value").text(convert(tazmachine.memory[addr], "instr"))      
    } else {
      $(".word[data-addr='"+addr+"'] > .value").text(convert(storage2tp()[addr], "instr"))      
    }
    views[addr] = "instr"
  });
  $int32.click(function(){
    $(".word[data-addr='"+addr+"'] > .views > .instr").toggleClass("vselected", false);
    $(".word[data-addr='"+addr+"'] > .views > .instr").toggleClass("vnotselected", true);
    $(".word[data-addr='"+addr+"'] > .views > .int32").toggleClass("vselected", true);
    $(".word[data-addr='"+addr+"'] > .views > .int32").toggleClass("vnotselected", false);
    $(".word[data-addr='"+addr+"'] > .views > .float").toggleClass("vselected", false);
    $(".word[data-addr='"+addr+"'] > .views > .float").toggleClass("vnotselected", true);
    if (exec) {
      $(".word[data-addr='"+addr+"'] > .value").text(convert(tazmachine.memory[addr], "int32"))      
    } else {
      $(".word[data-addr='"+addr+"'] > .value").text(convert(storage2tp()[addr], "int32"))      
    }
    views[addr] = "int32"
  });
  $float.click(function(){
    $(".word[data-addr='"+addr+"'] > .views > .instr").toggleClass("vselected", false);
    $(".word[data-addr='"+addr+"'] > .views > .instr").toggleClass("vnotselected", true);
    $(".word[data-addr='"+addr+"'] > .views > .int32").toggleClass("vselected", false);
    $(".word[data-addr='"+addr+"'] > .views > .int32").toggleClass("vnotselected", true);
    $(".word[data-addr='"+addr+"'] > .views > .float").toggleClass("vselected", true);
    $(".word[data-addr='"+addr+"'] > .views > .float").toggleClass("vnotselected", false);
    if (exec) {
      $(".word[data-addr='"+addr+"'] > .value").text(convert(tazmachine.memory[addr], "float"))      
    } else {
      $(".word[data-addr='"+addr+"'] > .value").text(convert(storage2tp()[addr], "float"))      
    }
    views[addr] = "float"
  });
  if (exec && tazmachine.addr == addr && tazmachine.halt) {
      $index.addClass("halt")
  }
  if (exec && tazmachine.addr == addr) {
      $index.addClass("ip")
      $newword.click(function(){
        $.post("/step", JSON.stringify(tazmachine), function(r){
          tazmachine = r.tm;
          drawUI(true);
        })
      })
  }
  if (!exec) {
    $effects = $("<div>", {class:'effects'});
    $modify = $("<button>", {class:'modify'});
    $delete = $("<button>", {class:'delete'});
    $effects.append($modify);
    $effects.append($delete);
    $newword.append($effects);
    $modify.click(function(){
      tw = storage2tp()[addr];
      if (views[addr] == "instr") {
        $b0 = $("<select>",{class: "byte", "data-offset": 0});
        $b0.append($("<option>", {text:"NOP", value:fromOp("NOP"), selected: (toOp(tw[0]) == "NOP")}))
        $b0.append($("<option>", {text:"INI", value:fromOp("INI"), selected: (toOp(tw[0]) == "INI")}))
        $b0.append($("<option>", {text:"OUI", value:fromOp("OUI"), selected: (toOp(tw[0]) == "OUI")}))
        $b0.append($("<option>", {text:"INF", value:fromOp("INF"), selected: (toOp(tw[0]) == "INF")}))
        $b0.append($("<option>", {text:"OUF", value:fromOp("OUF"), selected: (toOp(tw[0]) == "OUF")}))
        $b0.append($("<option>", {text:"MOV", value:fromOp("MOV"), selected: (toOp(tw[0]) == "MOV")}))
        $b0.append($("<option>", {text:"SWP", value:fromOp("SWP"), selected: (toOp(tw[0]) == "SWP")}))
        $b0.append($("<option>", {text:"JMP", value:fromOp("JMP"), selected: (toOp(tw[0]) == "JMP")}))
        $b0.append($("<option>", {text:"JEQ", value:fromOp("JEQ"), selected: (toOp(tw[0]) == "JEQ")}))
        $b0.append($("<option>", {text:"JNE", value:fromOp("JNE"), selected: (toOp(tw[0]) == "JNE")}))
        $b0.append($("<option>", {text:"JLS", value:fromOp("JLS"), selected: (toOp(tw[0]) == "JLS")}))
        $b0.append($("<option>", {text:"JGS", value:fromOp("JGS"), selected: (toOp(tw[0]) == "JGS")}))
        $b0.append($("<option>", {text:"JLE", value:fromOp("JLE"), selected: (toOp(tw[0]) == "JLE")}))
        $b0.append($("<option>", {text:"JGE", value:fromOp("JGE"), selected: (toOp(tw[0]) == "JGE")}))
        $b0.append($("<option>", {text:"ADD", value:fromOp("ADD"), selected: (toOp(tw[0]) == "ADD")}))
        $b0.append($("<option>", {text:"SUB", value:fromOp("SUB"), selected: (toOp(tw[0]) == "SUB")}))
        $b0.append($("<option>", {text:"MUL", value:fromOp("MUL"), selected: (toOp(tw[0]) == "MUL")}))
        $b0.append($("<option>", {text:"DIV", value:fromOp("DIV"), selected: (toOp(tw[0]) == "DIV")}))
        $b0.append($("<option>", {text:"MOD", value:fromOp("MOD"), selected: (toOp(tw[0]) == "MOD")}))
        $b0.append($("<option>", {text:"AND", value:fromOp("AND"), selected: (toOp(tw[0]) == "AND")}))
        $b0.append($("<option>", {text:"OR ", value:fromOp("OR "), selected: (toOp(tw[0]) == "OR ")}))
        $b0.append($("<option>", {text:"XOR", value:fromOp("XOR"), selected: (toOp(tw[0]) == "XOR")}))
        $b0.append($("<option>", {text:"NOT", value:fromOp("NOT"), selected: (toOp(tw[0]) == "NOT")}))
        $b0.append($("<option>", {text:"SHF", value:fromOp("SHF"), selected: (toOp(tw[0]) == "SHF")}))
        $b0.append($("<option>", {text:"ROT", value:fromOp("ROT"), selected: (toOp(tw[0]) == "ROT")}))
        $b0.append($("<option>", {text:"FAD", value:fromOp("FAD"), selected: (toOp(tw[0]) == "FAD")}))
        $b0.append($("<option>", {text:"FSU", value:fromOp("FSU"), selected: (toOp(tw[0]) == "FSU")}))
        $b0.append($("<option>", {text:"FMU", value:fromOp("FMU"), selected: (toOp(tw[0]) == "FMU")}))
        $b0.append($("<option>", {text:"FDI", value:fromOp("FDI"), selected: (toOp(tw[0]) == "FDI")}))
        $b0.append($("<option>", {text:"ITF", value:fromOp("ITF"), selected: (toOp(tw[0]) == "ITF")}))
        $b0.append($("<option>", {text:"FTI", value:fromOp("FTI"), selected: (toOp(tw[0]) == "FTI")}))
        $b0.append($("<option>", {text:"HLT", value:fromOp("HLT"), selected: (toOp(tw[0]) == "HLT")}))
        $b1 = $("<input>", {class: "byte", type:"number", "data-offset": 1, min: 0, max: 255, value: tw[1]});
        $b2 = $("<input>", {class: "byte", type:"number", "data-offset": 2, min: 0, max: 255, value: tw[2]});
        $b3 = $("<input>", {class: "byte", type:"number", "data-offset": 3, min: 0, max: 255, value: tw[3]});
        $value = $(".word[data-addr='"+addr+"'] > .value")      
        $value.text("")
        $value.append($b0);
        $value.append($b1);
        $value.append($b2);
        $value.append($b3);
        $(".word[data-addr='"+addr+"'] > .effects > .modify").off("click").prop("class", "apply");
        $(".word[data-addr='"+addr+"'] > .effects > .delete").off("click").prop("class", "cancel");
        $(".word[data-addr='"+addr+"'] > .views > .int32").toggleClass("vnotselected", true);
        $(".word[data-addr='"+addr+"'] > .views > .float").toggleClass("vnotselected", true);
        $(".word[data-addr='"+addr+"'] > .views > .instr").prop("disabled", true);
        $(".word[data-addr='"+addr+"'] > .views > .int32").prop("disabled", true);
        $(".word[data-addr='"+addr+"'] > .views > .float").prop("disabled", true);
        $(".word[data-addr='"+addr+"'] > .effects > .apply").click(function(){
          ntw = []
          ntw.push(parseInt($(".word[data-addr='"+addr+"'] > .value > .byte[data-offset=0]").val()))
          ntw.push($(".word[data-addr='"+addr+"'] > .value > .byte[data-offset=1]").val() % 256)
          ntw.push($(".word[data-addr='"+addr+"'] > .value > .byte[data-offset=2]").val() % 256)
          ntw.push($(".word[data-addr='"+addr+"'] > .value > .byte[data-offset=3]").val() % 256)
          sessionStorage.setItem(addr, ntw);
          drawUI(false)
        });
      } else {
        $inp = $("<input>", {class: "byte", type:"number", value: convert(tw, views[addr])});
        $value = $(".word[data-addr='"+addr+"'] > .value")      
        $value.text("")
        $value.append($inp);
        $(".word[data-addr='"+addr+"'] > .effects > .modify").off("click").prop("class", "apply");
        $(".word[data-addr='"+addr+"'] > .effects > .delete").off("click").prop("class", "cancel");
        if (views[addr] == "int32") {
          $(".word[data-addr='"+addr+"'] > .views > .instr").toggleClass("vnotselected", true);
          $(".word[data-addr='"+addr+"'] > .views > .float").toggleClass("vnotselected", true);
        } else {
          $(".word[data-addr='"+addr+"'] > .views > .instr").toggleClass("vnotselected", true);
          $(".word[data-addr='"+addr+"'] > .views > .int32").toggleClass("vnotselected", true);
        }
        $(".word[data-addr='"+addr+"'] > .views > .instr").prop("disabled", true);
        $(".word[data-addr='"+addr+"'] > .views > .int32").prop("disabled", true);
        $(".word[data-addr='"+addr+"'] > .views > .float").prop("disabled", true);
        $(".word[data-addr='"+addr+"'] > .effects > .apply").click(function(){
          sessionStorage.setItem(addr, num2tw($inp.val(), views[addr]));
          drawUI(false)
        });
      }
      $(".word[data-addr='"+addr+"'] > .effects > .cancel").click(function(){
        drawUI(false); // TODO: full redrawing is expensive, think about DOM manipulation
      });

    });
    $delete.click(function(){
      a = $.makeArray(sessionStorage);
      a.splice(addr, 1);
      views.splice(addr, 1)
      tp2storage(Object(a));
      drawUI(false); // TODO: full redrawing is expensive, think about DOM manipulation
    });
  }

  return $newword;
}

function newInput(i, ei){
  $i = $("<li>", {class: "input io", "data-index": i});
  $t = $("<button>", {class: "type"})
  $v = $("<div>", {class: "valueIO"})
  $i.append($t);
  $i.append($v);
  $v.text(ei.contents);
  switch (ei.tag) {
    case "II": {
      $t.text("Int32");
      break;
    }
    case "IF": {
      $t.text("Float");
      break;
    }
  }
  $t.click(function(){
    if (tazmachine.input[i].tag == "II") {
      $(".input.io[data-index="+i+"] > .type").text("Float");
      $(".input.io[data-index="+i+"] > .valueIO").text("0.0");
      tazmachine.input[i] = {tag: "IF", contents: 0.0};
    } else {
      $(".input.io[data-index="+i+"] > .type").text("Int32");
      $(".input.io[data-index="+i+"] > .valueIO").text("0");
      tazmachine.input[i] = {tag: "II", contents: 0};
    }
  });
  $v.click(function(){
    $t.prop("disabled", true);
    $ni = $("<input>", {class: "ni", type: "number", value: tazmachine.input[i].contents});  
    $effects = $("<div>", {class:'effects', style: "flex-basis: 10%; height: 100%;"});
    $apply = $("<button>", {class:'apply', style: "background-color: #333333"});
    $cancel = $("<button>", {class:'cancel', style: "background-color: #333333"});
    $effects.append($apply);
    $effects.append($cancel);
    $(".input.io[data-index="+i+"] > .valueIO").remove();
    $(".input.io[data-index="+i+"]").append($ni).append($effects);
    $apply.click(function(){
      switch (tazmachine.input[i].tag) {
        case "II": {
          tazmachine.input[i].contents = parseInt($(".input.io[data-index="+i+"] > .ni").val())
        }
        case "IF": {
          tazmachine.input[i].contents = parseFloat($(".input.io[data-index="+i+"] > .ni").val())
        }
      }
      drawUI(true); // TODO: full redrawing is expensive, think about DOM manipulation
    });
    $cancel.click(function(){
      drawUI(true); // TODO: full redrawing is expensive, think about DOM manipulation
   });
  });
  return $i;

}

function newOutput(i, eo){
  $o = $("<li>", {class: "output io", "data-index": i});
  $t = $("<div>", {class: "type", style: "border: none none none none;"})
  $v = $("<div>", {class: "valueIO", style: "border-right: 5px; border-right-style: solid; border-right-color: #4a4a4a"})
  $o.append($v);
  $o.append($t);
  $v.text(eo.contents);
  switch (eo.tag) {
    case "OI": {
      $t.text("Int32");
      break;
    }
    case "OF": {
      $t.text("Float");
      break;
    }
  }
  return $o;
}

// Helpers

function tp2storage(tp) {
  sessionStorage.clear();
  $.each(tp, function(k, v){sessionStorage.setItem(k, v)});
}

function storage2tp() {
  tp = {};
  $.each(sessionStorage, function(k,v){
    a = v.split(",");
    $.each(a, function(i, e) {a[i]=parseInt(e)})
    tp[k] = a;
  });
  return tp;
}

function convert(tw, towhat) {
  var data = tw;
      buf = new ArrayBuffer(4);
      view = new DataView(buf);
  data.forEach(function(i, e){view.setUint8(e, i)})
  if (towhat == "instr") {
    return toOp(view.getUint8(0)) +' '+ view.getUint8(1) +' '+ view.getUint8(2) +' '+ view.getUint8(3)
  } else if (towhat == "int32") {
    return view.getInt32(0);
  } else if (towhat == "float") {
    return view.getFloat32(0);
  }
}

function num2tw (num, from) {
  buf = new ArrayBuffer(4);
  view = new DataView(buf);
  if (from == "int32") {view.setInt32(0, num)} else if (from == "float") {view.setFloat32(0,num)}
  return [view.getUint8(0), view.getUint8(1), view.getUint8(2), view.getUint8(3)]
}

function showMessage(msg) {
  $msg = $("<div>", {class:"message"})
  $("body").prepend($msg);
  $msg.text(msg);
  $msg.click(function(){
    $msg.slideUp(500, function(){$msg.remove()});
  });
  $msg.slideDown(500);
}

function toOp(h) {
  switch (h) {
    case 0x00: return "NOP";
    case 0x01: return "INI";
    case 0x02: return "OUI";
    case 0x03: return "INF";
    case 0x04: return "OUF";
    case 0x10: return "MOV";
    case 0x11: return "SWP";
    case 0x12: return "JMP";
    case 0x13: return "JEQ";
    case 0x14: return "JNE";
    case 0x15: return "JLS";
    case 0x16: return "JGS";
    case 0x17: return "JLE";
    case 0x18: return "JGE";
    case 0x20: return "ADD";
    case 0x21: return "SUB";
    case 0x22: return "MUL";
    case 0x23: return "DIV";
    case 0x24: return "MOD";
    case 0x30: return "AND";
    case 0x31: return "OR ";
    case 0x32: return "XOR";
    case 0x33: return "NOT";
    case 0x34: return "SHF";
    case 0x35: return "ROT";
    case 0x40: return "FAD";
    case 0x41: return "FSU";
    case 0x42: return "FMU";
    case 0x43: return "FDI";
    case 0x50: return "ITF";
    case 0x51: return "FTI";
    case 0xff: return "HLT";
    default:   return "UNK";
  }
}

function fromOp(h) {
  switch (h) {
    case "NOP": return 0x00;
    case "INI": return 0x01;
    case "OUI": return 0x02;
    case "INF": return 0x03;
    case "OUF": return 0x04;
    case "MOV": return 0x10;
    case "SWP": return 0x11;
    case "JMP": return 0x12;
    case "JEQ": return 0x13;
    case "JNE": return 0x14;
    case "JLS": return 0x15;
    case "JGS": return 0x16;
    case "JLE": return 0x17;
    case "JGE": return 0x18;
    case "ADD": return 0x20;
    case "SUB": return 0x21;
    case "MUL": return 0x22;
    case "DIV": return 0x23;
    case "MOD": return 0x24;
    case "AND": return 0x30;
    case "OR ": return 0x31;
    case "XOR": return 0x32;
    case "NOT": return 0x33;
    case "SHF": return 0x34;
    case "ROT": return 0x35;
    case "FAD": return 0x40;
    case "FSU": return 0x41;
    case "FMU": return 0x42;
    case "FDI": return 0x43;
    case "ITF": return 0x50;
    case "FTI": return 0x51;
    case "HLT": return 0xff;
    default   : return 0x00;
  }
}
